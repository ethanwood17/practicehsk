//initializations
let cards = [];
let activeCard = 0;

// Get cookies
getCookie("theme") === "" ? setTheme("dark") : setTheme(getCookie("theme"));
getCookie("levels") === "" ? setLevel(1) : setLevel(getCookie("levels"));

function flipCard() {
  const el = document.querySelector('#card').classList.toggle('flipped');
}

function getNextCard() {
    activeCard++;
    setActiveCard();
}

function getLastCard() {
    activeCard--;
    setActiveCard();
}

function setActiveCard() {
    document.getElementById("character").innerHTML = cards[activeCard].simplified;
    document.getElementById("pinyin").innerHTML = cards[activeCard].pinyin;
    document.getElementById("definition").innerHTML = cards[activeCard].definition;
}

function updateSelected(level) {
    document.querySelectorAll("#level > span").forEach(e => (e.style.borderBottom = ""));
    document.querySelector(`#lv-${level}`).style.borderBottom = "2px solid";
}

function setLevel(level) {
  updateSelected(level);
  fetchLevel(level);
}

let checkbox = document.getElementById("theme-check");
checkbox.addEventListener('change', function() {
    if(this.checked) {
        setTheme("light");
    } else {
        setTheme("dark");
    }
    checkbox.blur();
});

function setTheme(_theme) {
  let theme = _theme;
  fetch(`themes/${theme}.css`)
    .then(response => {
      if (response.status === 200) {
        response
          .text()
          .then(css => {
            setCookie('theme', theme, 90);
            document.querySelector('#theme').setAttribute('href', `themes/${theme}.css`);
          })
          .catch(err => console.error(err));
      } else {
        console.log(`theme ${theme} is undefined`);
      }
    })
    .catch(err => console.error(err));
}

//navigation keys
document.addEventListener('keydown', e => {
  switch(e.keyCode) {
    case 32: flipCard(); break;
    case 37: getLastCard(); break;
    case 39: getNextCard(); break;
  }
});


function fetchLevel(level) {
    fetch(`cards/${level}.json`)
      .then((response) => response.json())
      .then((json) => {
        if (typeof json !== 'undefined') {
          cards = json;
          shuffleDeck();
          setCookie("levels", level, 90);
          updateSelected(level);
          setActiveCard();
        } else {
          console.error(`level ${level} is undefined`);
        }
      }).catch(err => console.error(err));
}

function shuffleDeck() {
    for(let i = cards.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * i);
        const temp = cards[i];
        cards[i] = cards[j];
        cards[j] = temp;
    }
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}